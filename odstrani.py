import re

def odstrani_koncnico(podatek):
    """ Odstrani končnico podatkom, da bo lažje pregledovanje podatkov v nadaljnem."""
    p = re.compile('[ 0-9]+')
    m = p.match(podatek)
    return int(m.group())

def odstrani_podatek(podatek):
    """ To funkcijo bomo uporabili, da odstranimo podatek o višini hribov iz okolice posameznega hriba. Služi lažjemu pregledovanju podatkov."""
    p = re.compile('[čČšŠžŽa-zA-Z\s\/\(\)]+')
    m = p.match(podatek)
    return m.group()[:-1]


