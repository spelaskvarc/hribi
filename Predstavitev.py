import sqlite3
import os
import sys
import csv
import re 
import codecs
import Ustvari_Tabelo as UT

#Datoteka z funkcijami na bazi.

def inicializiraj():

    #ustvarimo bazo in tabelo hribi_tabela in hribovja
    print("Polnim tabelo hribi_tabela")
    UT.napolni_tabelo()

    # ustvarimo tabelo je_okolica
    print("Polnim tabelo je_okolica")
    UT.okolica_tabela()

    #Ustvarimo tabelo poti
    print("Polnim tabelo poti")
    UT.poti_tabela()
    

########## Pomozne funkcije ################

def hrib_po_id(idH):
    baza = sqlite3.connect("hribi_baza.db")
    cursor = baza.cursor()
    cursor.execute("""SELECT * FROM hribi_tabela WHERE id=?""",(idH,))
    hrib = cursor.fetchone()[1::]
    cursor.execute("""SELECT ime_hribovja FROM hribovja WHERE id = ?""",(hrib[1],))
    hrib = (hrib[0],cursor.fetchone()[0],hrib[2],hrib[3],hrib[4])
    return hrib

def preberi_hribovje(st):
    baza = sqlite3.connect("hribi_baza.db")
    cursor = baza.cursor()
    cursor.execute("""SELECT ime_hribovja FROM hribovja WHERE id = ?""",(st,))
    return cursor.fetchone()[0]












def funkcije():
    print("Uporabiš lahko:")
    print("poisci_hrib")
    print("poisci_okolico")
    print("okolica_z_podatki")
    print("izberi_hrib_po_velikosti")
    print("poisci_poti")
    print("poti_dolzne_najvec")

def poisci_hrib(ime = "Triglav",izpis = True):
    baza = sqlite3.connect("hribi_baza.db")
    cursor = baza.cursor()
    cursor.execute("""SELECT * FROM hribi_tabela WHERE ime=?""",(ime,))
    hrib1 = cursor.fetchone()
    if hrib1 != None:
        hrib = hrib1[1::]
        cursor.execute("""SELECT ime_hribovja FROM hribovja WHERE id = ?""",(hrib[1],))
        hrib = (hrib[0],cursor.fetchone()[0],hrib[2],hrib[3],hrib[4])
        if izpis:
            print("(ime, hribovje, višina [m], trenutna_temperatura [°C], priljubljenost)\n")
            print(hrib)
        if izpis == False:
            return hrib1[0]
    else:
        print("Ups, to je pa nerodno.")
        print("Vašega hriba ni bilo mogoče najti.")

    
def poisci_okolico(ime= "Triglav"):
    baza = sqlite3.connect("hribi_baza.db")
    cursor = baza.cursor()
    cursor.execute("""SELECT id FROM hribi_tabela WHERE ime=?""",(ime,))
    poizvedba = cursor.fetchone()
    if poizvedba == None:
        print("Hriba z tem imenom ni.")
        return
    
    idH = poizvedba[0]
    cursor.execute('''SELECT * FROM je_okolica WHERE hrib=? ''',(idH,))
    if cursor.fetchone() != None:
        
        print("V okolici hriba",ime,"so:")
        for i in cursor.fetchall():
            (h1,h2) = i
            cursor.execute("""SELECT ime FROM hribi_tabela WHERE id=?""",(h1,))
            hh1 = cursor.fetchone()[0]
            cursor.execute("""SELECT ime FROM hribi_tabela WHERE id=?""",(h2,))
            hh2 = cursor.fetchone()[0]
            print("-",hh2)
    else:
        print("Ta hrib ne obstaja ali pa nima okoliških hribov.")


def okolica_z_podatki(ime="Triglav"):
    baza = sqlite3.connect("hribi_baza.db")
    cursor = baza.cursor()
    cursor.execute("""SELECT id FROM hribi_tabela WHERE ime=?""",(ime,))
    poizvedba = cursor.fetchone()
    if poizvedba != None:
        idH = poizvedba[0]
        print("V okolici hriba",ime,"so:")
        
        cursor.execute('''SELECT * FROM je_okolica WHERE hrib=? ''',(idH,))
        for i in cursor.fetchall():
            (h1,h2) = i
            okoliski = hrib_po_id(h2)
            print("-",okoliski)
    else:
        print("Ta hrib ne obstaja ali pa nima okoliških hribov")


#(ime, hribovje, višina [m], trenutna_temperatura [°C], priljubljenost)


def izberi_hrib_po_velikosti(v1,v2):
    baza = sqlite3.connect("hribi_baza.db")
    cursor = baza.cursor()
    cursor.execute("""SELECT * FROM hribi_tabela WHERE višina <=? AND višina >=?""",(v2,v1))
    hribi = cursor.fetchall()
    if hribi != []:
        for j in range(len(hribi)):
            i = hribi[j]
            k = (i[1], preberi_hribovje(i[2]),i[3],i[4],i[5])
            hribi[j] = k
        print("Hribi visoki med",v1,"in",v2,"metrov so:")
        print("(ime, hribovje, višina [m], trenutna_temperatura [°C], priljubljenost)\n")
        for hrib in hribi:
            print(hrib)
    else:
        print("Nismo našli hriba ki bi bil visok med",v1,"in",v2,"metrov.")

        

def poisci_poti(ime="Triglav"):
    baza = sqlite3.connect("hribi_baza.db")
    cursor = baza.cursor()
    cursor.execute("""SELECT * FROM poti WHERE hrib=?  """,(poisci_hrib(ime,False),))
    poti = cursor.fetchall()

    for j in range(len(poti)):
        i = poti[j]
        k = (i[2],i[3],i[4])
        poti[j] = k
        
    print("Poti na",ime,"so:")
    print("(ime_poti, dolžina [min], težavnost)\n")
    for i in poti:
        print("-",i)


def poti_dolzne_najvec(cas="60",ime="Triglav"):
    baza = sqlite3.connect("hribi_baza.db")
    cursor = baza.cursor()
    cursor.execute("""SELECT * FROM poti WHERE hrib=? AND dolžina <=? """,(poisci_hrib(ime,False),cas))
    poti = cursor.fetchall()

    for j in range(len(poti)):
        i = poti[j]
        k = (i[2],i[3],i[4])
        poti[j] = k

    if len(poti) != 0:
        print("Poti na",ime,"so:")
        print("(ime_poti, dolžina [min], težavnost)\n")
        for i in poti:
            print("-",i)
    else:
        print("Poti, ki bi bila dolga največ",cas,"minut, ni mogoče najti.")

    












        
    
